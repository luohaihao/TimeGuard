﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace TimeGuard
{
    class Common
    {

        public static void LogText(string content)
        {
            try
            {
                //System.IO.File.AppendAllText(System.Reflection.Assembly.GetExecutingAssembly().Location  + DateTime.Now.Year + "-" + DateTime.Now.Month + ".log" , DateTime.Now + ", " +log +Environment.NewLine);
                string folder = "";
                folder = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                string logFile = "";
                logFile = folder + DateTime.Now.Year + "-" + DateTime.Now.Month + ".log";
                System.IO.File.AppendAllText(logFile, DateTime.Now + ", " + content + Environment.NewLine);

            }
            catch(Exception e)
            {
                System.Console.WriteLine("LogText()-->" + e.ToString() + "\n");
            }
        }



        public  void EnsureUserIsAdmin()
        {
            WindowsPrincipal principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
            {
                RelaunchAsAdmin();
            }
        }

        public void RelaunchAsAdmin()
        {
            try
            {

                string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                ProcessStartInfo psi = new ProcessStartInfo(exePath);
                psi.Verb = "runas";
                Process.Start(psi);
            }
            catch (Exception e)
            {
                Common comm = new Common();
                LogText("RelaunchAsAdmin()-->" + e.ToString());
            }

            Environment.Exit(0);
        }


    }
}
