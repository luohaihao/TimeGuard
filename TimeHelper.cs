﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Runtime;
using TimeGuard;

/// <summary>  
/// 更新系统时间  
/// </summary>  
public class TimeHelper
{

    private TimeGuard.Common comm = new TimeGuard.Common();

    //设置系统时间的API函数  
    [DllImport("kernel32.dll")]
    private static extern bool SetLocalTime(ref SYSTEMTIME time);

    [StructLayout(LayoutKind.Sequential)]

    private struct SYSTEMTIME
    {
        public short year;
        public short month;
        public short dayOfWeek;
        public short day;
        public short hour;
        public short minute;
        public short second;
        public short milliseconds;
    }


    /// <summary>
    /// Get Time from the response(Header) of www.baidu.com or www.360.cn
    /// </summary>
    /// <returns></returns>
    public DateTime GetNetWorkTime()
    {
        DateTime netTime = DateTime.Parse("2017-1-1 00:00:00"); //init;
        netTime = GetBaiduTime();
        if (!IsValidTime(netTime))
        {
            netTime = Get360Time();
        }
        return netTime;
    }


    /// <summary>
    /// Get Time from the response of www.baidu.com 
    /// </summary>
    /// <returns></returns>
    public DateTime GetBaiduTime()
    {
        DateTime netTime = DateTime.Parse("2017-1-1 00:00:00"); //init;
        try
        {
            string url = "http://www.baidu.com";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";
            request.Timeout = 5000;
            //request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string gmtDate = response.Headers.Get("Date");

            DateTime convertedDate = DateTime.Parse(gmtDate);
            return convertedDate;

        }
        catch (Exception e)
        {
            
            //TimeGuard.Common comm = new TimeGuard.Common();
            Common.LogText("GetBaiduTime()-->" + e.ToString());
        }
        return netTime;
    }


    /// <summary>
    /// Get Time from the response(header) of www.360.cn
    /// </summary>
    /// <returns></returns>
    public DateTime Get360Time()
    {
        DateTime netTime = DateTime.Parse("2017-1-1 00:00:00");
        try
        {
            string url = "http://www.360.cn";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";
            request.Timeout = 5000;
            //request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string gmtDate = response.Headers.Get("Date");

            DateTime convertedDate = DateTime.Parse(gmtDate);
            return convertedDate;

        }
        catch (Exception e)
        {
            Common.LogText("Get360Time()-->" + e.ToString());
        }
        return netTime;
    }


    /// <summary>  
    /// 设置系统时间  
    /// </summary>  
    /// <param name="dt">需要设置的时间</param>  
    /// <returns>返回系统时间设置状态，true为成功，false为失败</returns>  
    public bool SetSystemDate(DateTime dt)
    {
        SYSTEMTIME st;

        st.year = (short)dt.Year;
        st.month = (short)dt.Month;
        st.dayOfWeek = (short)dt.DayOfWeek;
        st.day = (short)dt.Day;
        st.hour = (short)dt.Hour;
        st.minute = (short)dt.Minute;
        st.second = (short)dt.Second;
        st.milliseconds = (short)dt.Millisecond;
        bool rt = SetLocalTime(ref st);
        return rt;
    }


    public bool IsValidTime(DateTime mytime)
    {
        bool isOk = false;
        TimeSpan ts = mytime - DateTime.Parse("2017-1-1 00:00:00");
        if (ts.TotalSeconds < 3600) //failed to get 360 time so the span is smaller than 1 hour
        {
            isOk = false;
        }
        else { isOk = true; }
       
        return isOk;

    }


}
