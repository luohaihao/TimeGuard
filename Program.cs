﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Net;
using System.Diagnostics;
using System.Security.Principal;
using Topshelf;

namespace TimeGuard
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.OnException((ex) =>
               {
                   Common.LogText("服务异常："+ ex.ToString());
               });
                x.Service<BLL>(s =>
                {
                    s.ConstructUsing(name => new BLL());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });

                x.RunAsLocalService();
                x.StartAutomatically();


                x.SetDescription("Auto adjust time from baidu and 360");
                x.SetDisplayName("TimeGuard");
                x.SetServiceName("TimeGuard Service");

            });
            

        }



    }

}
