﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeGuard
{
    class BLL
    {

        public void Start()
        {
            Common.LogText("Application Start");
            RefreshTime();
            GC.Collect();
        }

        public void Stop()
        {
            Common.LogText("Application End");
        }

        public void RefreshTime()
        {
            
            TimeHelper helper = new TimeHelper();

            DateTime netWorkTime = DateTime.Parse("2017-1-1 00:00:00"); //init

            bool isCompleted = false;
            int sleepInterval = 5; //initial interval is 5s ,then 5-->10-->20-->40-->80...

            while (!isCompleted)
            {


                netWorkTime = helper.GetNetWorkTime();
                if (helper.IsValidTime(netWorkTime))
                {
                    isCompleted = helper.SetSystemDate(netWorkTime);
                    Common.LogText("Now Time:" + netWorkTime.ToString());
                    System.Console.WriteLine("Set System Time:" + netWorkTime.ToString());
                }

                if (!isCompleted)
                {
                    System.Console.WriteLine(String.Format("Unfinished! Sleep {0} second", sleepInterval));
                    Common.LogText(String.Format("Unfinished! Sleep {0} second", sleepInterval));
                    System.Threading.Thread.Sleep(sleepInterval * 1000);
                    sleepInterval = sleepInterval * 2;
                }

            }
        }

    }
}
